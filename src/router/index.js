import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/peopleList",
    name: "PeopleList",
    component:  () => import("../views/PeopleList")
  },
  {
    path: "/peopleSearch",
    name: "PeopleSearch",
    component: () => import("../views/PeopleSearch")
  },
  {
    path: "/filmList",
    name: "FilmList",
    component:  () => import("../views/FilmList")
  }
];

const router = new VueRouter({
  mode: 'history',
  routes
});

export default router;
